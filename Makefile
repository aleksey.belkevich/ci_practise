all: compile link


compile:
	gcc -o main.o -c main.c

link:
	gcc -o test main.o


clean:
	rm -f ./test
	rm -f ./main.o
